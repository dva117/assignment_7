# Add executable called "helloDemo" that is built from the source files
# "demo.cxx" and "demo_b.cxx". The extensions are automatically found.
add_executable (shoppinglist main.c ShoppingList.c)
set_property(TARGET shoppinglist PROPERTY C_STANDARD 99)
target_compile_options(shoppinglist PRIVATE -Wall -Wextra -pedantic)
