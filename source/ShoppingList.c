#define _CRT_SECURE_NO_WARNINGS
#include"ShoppingList.h"
#include<stdio.h>
#include<string.h>
#include<stdlib.h> // For malloc() and free()
#define F_LENGTH 32
//==============================================================================
struct GrocaryItem* allocItem(struct ShoppingList *list, int more)
{
    return (struct GrocaryItem*) realloc((*list).itemList,
            (*list).length * sizeof(struct GrocaryItem) + sizeof(struct GrocaryItem) * more);
}
//==============================================================================
void addItem(struct ShoppingList *list)
{
    if (((*list).itemList = allocItem(list, 1)) != NULL)
    {
        printf("Enter name: ");
        scanf("%s", (*list).itemList[(*list).length].productName);			
        do
        {
            printf("Enter amount: ");
            scanf("%f", &(*list).itemList[(*list).length].amount);
        } while ((*list).itemList[(*list).length].amount < 0);
        printf("Enter unit: ");
        scanf("%s", (*list).itemList[(*list).length].unit);
        printf("\n%s was added to the shopping list", (*list).itemList[(*list).length].productName);
        (*list).length++;
    }
    else
    {
        printf("Unable to allocate memory");
    }
}
//==============================================================================
void printList(struct ShoppingList *list)
{
    int i;
    float time = 0;

    if ((*list).length < 1)
    {
        printf("\nYour shopping list is empty");
    }

    else
    {
        printf("\nShopping list: \n");
        for(i = 0; i < (*list).length; i++)
        {
            printf("% 3d. %-20s %6.2f  %s\n", i+1, (*list).itemList[i].productName,
                    (*list).itemList[i].amount,
                    (*list).itemList[i].unit);
        }

        for(i = 0; i < (*list).length; i++)
        {
            time = time + 3 + (*list).itemList[i].amount;  
            //handeling this like a float since it doesnt say anywhere that we are not allowed to do that
            //and amount can be a float and nothing says we are to treat this like an int 
            //besides the +1s for each unit but the program has always been handled to be able to get for example '2.5l milk'
        }
        printf("\nIt will take %.1f seconds to process these items.", time);
    }
}
//==============================================================================
void editItem(struct ShoppingList *list)
{
    int item;

    if ((*list).length <= 0)
    {
        printf("\nYour shopping list doesn't contain any items");    
    }

    else
    {
        printList(list);    
        printf("\nWhich item do you wish to change?: ");
        scanf("%d", &item);
        item = item - 1;

        if ((item >= (*list).length) || (item < 0))
        {
            printf("\nYour list only contains %d items", (*list).length);
        }
        else
        {
            printf("\nCurrent amount: %.2f %s", (*list).itemList[item].amount,
                    (*list).itemList[item].unit);
            printf("\nEnter new amount: ");        
            scanf("%f", &(*list).itemList[item].amount);
        }
    }
}
//==============================================================================
void removeItem(struct ShoppingList *list)
{
    int i;
    int item;

    if ((*list).length <= 0)
    {
        printf("\nYour shopping list doesn't contain any items");
    }

    else
    {
        printList(list); 
        printf("\nWhich item do you wish to remove?: ");
        scanf("%d", &item);
        item = item - 1;

        if ((item >= (*list).length) || (item < 0))
        {
            printf("\nYour list only contains %d items", (*list).length);
        }

        else
        {
            printf("\n%s has been removed from the shopping list",
                    (*list).itemList[item].productName);
            for (i = item; i < (*list).length; i++)
            {
                strcpy((*list).itemList[i].productName, (*list).itemList[i+1].productName);
                (*list).itemList[i].amount = (*list).itemList[i+1].amount;
                strcpy((*list).itemList[i].unit, (*list).itemList[i+1].unit);
            }
            (*list).length--;
        }
        if (((*list).itemList = allocItem(list, 0)) == NULL)
        {
            printf("reallocation of memory failed");
        }
    }
}
//==============================================================================
void saveList(struct ShoppingList *list)
{
    char save_file[F_LENGTH];
    FILE *fp;
    int i;
    char file_search[F_LENGTH];
    char *temp;
    char *file_type;
    char date[F_LENGTH];

    printf("\nEnter save file name('filename.type', '.txt' for text file or '.bin' for bin file): ");
    scanf("%s", save_file);
    printf("\nEnter today's date: ");
    scanf("%s", date);
    strcpy(file_search, save_file);

    temp = strtok(file_search, ".");
    while((temp = strtok(NULL, ".")) != NULL)
    {
        file_type = temp;
    }

    if (strcmp("txt", file_type) == 0)
    {
        fp = fopen(save_file, "w+");
        if (fp != NULL)
        {
            fprintf(fp, "%s\n", date);
            for(i = 0; i < (*list).length; i++)
            {
                fprintf(fp, "%s %f %s\n", (*list).itemList[i].productName,
                        (*list).itemList[i].amount, 
                        (*list).itemList[i].unit);
            }
            fclose(fp);
            printf("\nList saved to file %s", save_file);
        }
    }

    else if (strcmp("bin", file_type) == 0)
    {
        fp = fopen(save_file, "wb");
        if (fp != NULL)
        {
            fwrite(date, sizeof(char), F_LENGTH, fp);
            fwrite((*list).itemList, sizeof(struct GrocaryItem), (*list).length, fp);
            fclose(fp);
            printf("\nList saved to file %s", save_file);
        }
    }


    else
    {
        printf("\nUnable to save to file %s", save_file);
    }
}
//==============================================================================
void loadList(struct ShoppingList* list)
{
    char load_file[F_LENGTH];
    FILE *fp;
    int i = 0;
    char file_search[F_LENGTH];
    char *temp;
    char *file_type;
    char date[F_LENGTH];

    printf("\nEnter load file name('filename.type', '.txt' for text file or '.bin' for bin file): ");
    scanf("%s", load_file);
    strcpy(file_search, load_file);

    temp = strtok(file_search, ".");
    while((temp = strtok(NULL, ".")) != NULL)
    {
        file_type = temp;
    }

    if (strcmp("txt", file_type) == 0)
    {
        fp = fopen(load_file, "r");
        if (fp != NULL)
        {

            free((*list).itemList);
            (*list).itemList = NULL;    
            (*list).length = i;
            fscanf(fp, "%s", date);
            if (((*list).itemList = allocItem(list, 1)) == NULL)
            {
                printf("Memory allocation for loaded file failed, loading of file canceled");
                return;
            }
            while(fscanf(fp, "%s %f %s", (*list).itemList[i].productName, 
                        &(*list).itemList[i].amount,
                        (*list).itemList[i].unit) > 0)
            { 
                i++;
                (*list).length = i;
                if (((*list).itemList = allocItem(list, 1)) == NULL)
                {
                    printf("Memory allocation for loaded file failed, loading of file canceled");
                    return;
                }
            }
            (*list).length = i;
            fclose(fp);
            printf("\nFile %s has been loaded", load_file);
            printf("\nList loaded. It was created on %s\n", date);
        }
    }


    else if (strcmp("bin", file_type) == 0)
    {
        fp = fopen(load_file, "rb");
        if (fp != NULL)
        {
            free((*list).itemList);
            (*list).itemList = NULL;
            (*list).length = i;
            fread(date, sizeof(char), F_LENGTH, fp);
            if (((*list).itemList = allocItem(list, 1)) == NULL)
            {
                printf("Memory allocation for loaded file failed, loading of file canceled");
                return;
            }
            while(fread(&(*list).itemList[i], sizeof(struct GrocaryItem), 1, fp) > 0)
            {
                i++;
                (*list).length = i;
                if (((*list).itemList = allocItem(list, 1)) == NULL)
                {
                    printf("Memory allocation for loaded file failed, loading of file canceled");
                    return;
                }

            }
            fclose(fp);
            printf("\nFile %s has been loaded", load_file);
            printf("\nList loaded. It was created on %s\n", date);
        }
    }


    else
    {
        printf("\nUnable to load file %s", load_file);
    }
}
